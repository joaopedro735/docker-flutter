FROM ghcr.io/cirruslabs/android-sdk:34

USER root

ARG flutter_version=stable
ARG android_build_tools_version=30.0.3

ENV FLUTTER_HOME=${HOME}/sdks/flutter \
    FLUTTER_VERSION=$flutter_version
ENV FLUTTER_ROOT=$FLUTTER_HOME

ENV PATH ${PATH}:${FLUTTER_HOME}/bin:${FLUTTER_HOME}/bin/cache/dart-sdk/bin

RUN git clone --depth 1 --branch ${FLUTTER_VERSION} https://github.com/flutter/flutter.git ${FLUTTER_HOME}

RUN dart --disable-analytics \
    && flutter config --no-analytics --enable-android --no-enable-web --no-enable-linux-desktop --no-enable-macos-desktop --no-enable-windows-desktop \
    && yes | flutter doctor --android-licenses \
    && flutter doctor \
    && flutter precache --android \
    && sdkmanager "--update" "--verbose" \
    && yes "y" | sdkmanager "build-tools;$android_build_tools_version" \
    && yes "y" | sdkmanager "platforms;android-28" \
    && yes "y" | sdkmanager "platforms;android-30" \
    && yes "y" | sdkmanager "platforms;android-31" \
    && yes "y" | sdkmanager "platforms;android-33" \
    && yes "y" | sdkmanager "emulator" \
    && yes "y" | sdkmanager "platform-tools" \
    && chown -R root:root ${FLUTTER_HOME}

